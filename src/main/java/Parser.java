package main.java;

import java.util.regex.Pattern;
import static main.java.DataArray.nameCurrency;
import static main.java.DataArray.valueCurrency;

public class Parser
{
    public static void inPutValue(String name, String value) {
        value = value.trim();
        name = name.trim();

        try {
            if (Pattern.matches("[A-Z]*", name) && !name.equals("SAVE") && !name.equals("EXIT") &&
                                                             !name.equals("OPEN") && !name.equals("HELP") )
            {
                Boolean chekArr = true;
                if (Pattern.matches("[ ]?[+-]?[0-9]*", value)) {
                    Integer valueInt = Integer.parseInt(value);
                    for (int index = 0; index < nameCurrency.size(); index++) {
                        if (nameCurrency.get(index).equals(name)) {
                            synchronized (nameCurrency) {
                                valueCurrency.set(index, (valueCurrency.get(index) + valueInt));
                                chekArr = false;
                            }
                        }
                    }
                    if (chekArr) {
                        DataArray.recordData(name, valueInt);
                    }
                } else {
                    System.out.print("Wrong pair entry: ");
                    System.out.println("currency '"+name+"'"+" sum '"+value+"'");
                }
           } else {
                System.out.print("Wrong pair entry: ");
                System.out.println("currency '"+name+"'"+" sum '"+value+"'");
            }
        } catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
            System.out.print("Wrong pair entry: ");
            System.out.println("currency '"+name+"'"+" sum '"+value+"'");
        }
    }
}