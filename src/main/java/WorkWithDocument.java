package main.java;

import java.io.*;
import java.util.List;


public class WorkWithDocument
{

    public static void saveDocuments (List<String> nameCurrency, List<Integer> valueCurrency)
    {
        try(FileWriter writer = new FileWriter("saveprogress.txt", false))
        {
            for (int index = 0; index<nameCurrency.size(); index++)
            {
                writer.write(nameCurrency.get(index)+"\n");
                writer.write(valueCurrency.get(index)+"\n");
                writer.flush();

            }
        }
        catch(IOException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void openDocuments (String name, String value)
    {
        value = value.replaceAll(" ", "");
        File file = new File(value + ".txt");
        if (file.exists() && file.isFile())
        {
            boolean writeStream = true;
            try (BufferedReader br = new BufferedReader(new FileReader(value+".txt")))
            {
                String checkString;
                while ((checkString = br.readLine()) != null) {
                    if (writeStream) {
                        name = checkString;
                        writeStream = false;
                    } else {
                        value = checkString;
                        Parser.inPutValue(name, value);
                        writeStream = true;
                    }
                }
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        } else {
            System.out.println("File is not founded");
        }
    }
}