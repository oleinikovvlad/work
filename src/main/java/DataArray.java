package main.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public  class DataArray {
   static List<String> nameCurrency = Collections.synchronizedList(new ArrayList<>());
   static List<Integer> valueCurrency = Collections.synchronizedList(new ArrayList<>());

   public static void recordData (String name, Integer value) {
       synchronized (nameCurrency){
           nameCurrency.add(name);
           valueCurrency.add(value);
       }
   }
}
