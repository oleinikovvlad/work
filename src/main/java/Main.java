package main.java;

import java.util.Scanner;
import static main.java.DataArray.nameCurrency;
import static main.java.DataArray.valueCurrency;

public class Main
{
    public static void main(String[] args)
    {
        String textArr = "test";
        String numberArr = "saveprogress";

        WorkWithDocument.openDocuments (textArr, numberArr);
        new MessageHelp();

	    RepeatedOutput.repeatedOutput (nameCurrency,valueCurrency);

        while (true)
        {
            System.out.println("\n"+"Enter currency and amount:");
            Scanner scanner = new Scanner(System.in);
            textArr = scanner.next();
            numberArr = scanner.nextLine();

            switch (textArr)
            {
                case "exit":
                    WorkWithDocument.saveDocuments (nameCurrency, valueCurrency);
                    System.out.println("Progress successfully saved!");
                    System.out.println("Goodbye!");
                    System.exit(0);
                case "save":
                    WorkWithDocument.saveDocuments (nameCurrency,valueCurrency);
                    System.out.println("Progress successfully saved!");
                    break;
                case "open":
                    WorkWithDocument.openDocuments (textArr, numberArr);
                    break;
                case "help":
                    new MessageHelp();
                    break;
                default:
                    Parser.inPutValue (textArr,numberArr);
                    break;
            }
        }
    }
}
