package main.java;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RepeatedOutput
{
    public static void repeatedOutput (List<String> nameCurrency, List<Integer> valueCurrency)
    {
        final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleWithFixedDelay(new Runnable()  {
            public void run() {
                synchronized (nameCurrency){
                    System.out.println();
                    Iterator<String> name = nameCurrency.listIterator();
                    Iterator<Integer> value = valueCurrency.listIterator();
                    while (name.hasNext() && value.hasNext()) {
                        int check = value.next();
                        switch (check) {
                            case 0:
                                break;
                            default:
                                System.out.println(name.next() + " " + check);
                                break;
                        }
                    }
                    System.out.println("\n" + "Enter currency and amount:");
                }
            }
        }, 1, 1, TimeUnit.MINUTES);
    }
}
