package main.java;


public class MessageHelp
{
    {
        System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        System.out.println("* if you want to close this program write: exit                         *");
        System.out.println("* if you want to save this program write: save                          *");
        System.out.println("* if you want to open your document write: open 'name document'         *");
        System.out.println("* if you want to see this prompt and available currencies write: help   *");
        System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        String outNameCurrency = " ";
        boolean chekOut = false ;
        for (int index = 0; index< DataArray.nameCurrency.size(); index++)
        {
            outNameCurrency = outNameCurrency + DataArray.nameCurrency.get(index) + " ";
            chekOut = true;
        }
        if (chekOut)
        {
            System.out.println("Available currencies:");
            System.out.println(outNameCurrency +"\n");
        }
    }
}
